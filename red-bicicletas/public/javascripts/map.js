var mymap = L.map('myMap').setView([12.1374736, -86.3056755], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicleta.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap)
        });
    }
});