require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var passport = require('./config/passport');
var session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
var jwt = require('jsonwebtoken');
var newrelic = require('newrelic');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var tokenRouter = require('./routes/token');

const usuario = require('./models/usuario');
const token = require('./models/token');

var mongodb = process.env.MONGO_URI;
var store = new session.MemoryStore;

mongoose.connect(mongodb, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: 'true',
    secret: 'red-biclicletas()()()()()()()())'
}));

app.get('/login', function(req, res) {
    res.render('sessions/login');
});

app.post('/login', function(req, res, next) {
    // Passport
    passport.authenticate('local', function(err, user, info) {
        if (err) return next(err);
        if (!user) res.render('sessions/login', { info });
        req.login(user, function(err) {
            if (err) return next(err);
            return res.redirect('/');
        });
    })(req, res, next);
});

app.get('/logout', function(req, res) {
    req.logOut();
    res.redirect('/');
});

app.get('/forgotPassword', function(req, res) {
    res.render('sessions/resetPassword');
});

app.post('/forgotPassword', function(req, res) {

});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/token', tokenRouter);
app.use('/google30c53e907d145189', function(req, res) {
    res.sendFile(__dirname + '/public/google30c53e907d145189.html');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

function loggedIn(req, res, next) {
    if (req.user) {
        next();
    } else {
        console.log('User sin loguearse');
        res.redirect('/login');
    }
};

function validarUsuario(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
        if (err) {
            res.json({ status: "error", message: err.message, data: null });
        } else {
            res.body.userId = decoded.id;
            console.log('jwt verify ' + decoded);
            next();
        }
    });
}

module.exports = app;