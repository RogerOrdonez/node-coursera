var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(() => {
        res.status(200).json({
            bicicleta: Bicicleta.allBicis
        });
    });
}

exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta({ code: req.code, color: req.color, modelo: req.modelo });
    Bicicleta.add(bici, function(err, newBici) {
        if (err) console.log(err);
        res.status(200).json({
            bicicleta: newBici
        });
    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = function(req, res) {
    var bici = Bicicleta.findById(req.body.id);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.status(200).json({
        bicicleta: bici
    });
}